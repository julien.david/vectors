#ifndef __RANDOM_TEXT_GENERATOR_HPP
#define __RANDOM_TEXT_GENERATOR_HPP
#include<iostream>
#include"RandomDistributionGenerator.hpp"


template<typename F = ConcaveFunction>
class RandomTextGenerator{
private:
  RandomDistributionGenerator<F> rdg;
  long double * distribution;
  int alphabetsize;
  long double target;
  
public:
  RandomTextGenerator(const int & alphabetsize, const long double & target){
    this->distribution = new long double[alphabetsize];
    this->alphabetsize = alphabetsize;
    this->target = target;
    rdg.random(this->distribution, target, alphabetsize,
	       std::max(alphabetsize*alphabetsize,10000)); 
  }
  ~RandomTextGenerator(){
    delete [] this->distribution;
  }

  void random(char * res, int size, bool change_gen = false){
    int i;
    if(change_gen)
      rdg.random(this->distribution, this->target, alphabetsize,
		 std::max(alphabetsize*alphabetsize,10000)); 
    for(i = 0; i<size; i++)
      res[i] = 'a'+draw_according_to_distribution(this->distribution, this->alphabetsize);
    res[i] = '\0';
  }
  
};

#endif

