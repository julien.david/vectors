#ifndef __CONCAVE_FUNCTION_HPP__
#define __CONCAVE_FUNCTION_HPP__

#include<math.h>
#include<iostream>
#define EPSILON powl(2,-64)


class ConcaveFunction{
 
public:
  virtual int sign() = 0;
  virtual long double g(long double x, int k) = 0;
  virtual long double h(long double x, int k) = 0;
  virtual long double h_inv(long double x, int k) = 0;

  virtual long double contribution(long double * t, int n) = 0;

  long double aim(long double * t, int n){
    return h(contribution(t,n),n);
  }

  virtual long double update(long double t, long double * x, int k, int l) = 0;
  virtual long double update_target(long double t, long double t2, int k, int l) = 0;

  long double newton(long double (*function)(long double x, long double s, long double t),
		     long double (*derivate)(long double x, long double s),
		     long double s, long double t, long double x, long double max_step){
    int step = 0;
    for(; step < max_step && function(x,s,t) != 0; step++){
      x = x - function(x,s,t)/derivate(x,s);
      x = (x<0)? -x:x;
    }
    return x;
  }

  long double newton_max(long double s, long double t, long double x, long double max_step){
    int step = 0;
    for(; step < max_step && maxInv(x,s,t) != 0 && x>= EPSILON && x<= s; step++){
      x = x - maxInv(x,s,t)/diff_maxInv(x,s);
      x = (x<0)? -x:x;
    }
    return x;
  }

  long double newton_min(long double s, long double t, long double x, long double max_step){
    int step = 0;
    for(; step < max_step && minInv(x,s,t) != 0 && x>= EPSILON && x<= s; step++){
      //std::cout<<"x:"<<x<<" s:"<<s<<" diff"<<diff_minInv(x,s)<<" t:"<<t<<std::endl;
      x = x - minInv(x,s,t)/diff_minInv(x,s);
      x = (x<0)? -x:x;
    }
    return x;
  }

  long double aim_double(long double x1, long double x2){
    long double t[2] = {x1, x2};
    return aim(t, 2);
  }

  long double aim_triple(long double x1, long double x2, long double x3){
    long double t[3] = {x1, x2, x3};
    return aim(t, 3);
  }
  
  virtual long double aim_with_target(long double x, long double s, long double t) = 0;
  virtual long double diff_aim(long double x, long double s) = 0;

  virtual long double maximal_of_two_values(long double partial_sum)  = 0;  
  virtual long double maxInv(long double x, long double s, long double t) = 0;
  virtual long double diff_maxInv(long double x, long double s) = 0;

  virtual long double minimal_of_two_values(long double partial_sum) = 0;
  virtual long double minInv(long double x, long double s, long double t) = 0;
  virtual long double diff_minInv(long double x, long double s) = 0;

  ConcaveFunction(){}
  
};



#endif
