#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include"ShannonEntropy.hpp"
#include"RandomDistributionGenerator.hpp"

int main(int argc, char ** argv){
    srand48(time(NULL));
    srand(time(NULL));
    if(argc != 3){
        perror("./random_distribution event_number target");
        return EXIT_FAILURE;
    }
    int size = atoi(argv[1]);
    long double target = atof(argv[2]);
    long double * distribution = new long double[size];
    int i;    
    int nb_experiment = 1;
    int nb_steps = (size*size < 10000)? 10000:size*size;
    RandomDistributionGenerator<ShannonEntropy> rdg;
    
    for(i = 0; i < nb_experiment; i++){
      rdg.random(distribution, target, size, nb_steps);
      rdg.print_distribution(distribution, size);
    }
    delete [] distribution;
    return EXIT_SUCCESS;
}
