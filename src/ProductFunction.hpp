#ifndef __PRODUCT_FUNCTION_HPP__
#define __PRODUCT_FUNCTION_HPP__

#include"ProductConcaveFunction.hpp"


class ProductFunction: public ProductConcaveFunction{
public:

  int sign(){
    return -1;
  }
  
  long double g(long double x, int k){
    return x;
  }
  
  long double h(long double y, int k){
    return y;
  }

  long double h_inv(long double y, int k){
    return y;
  }
  
  long double aim_with_target(long double x, long double s, long double t){
    return x*(s-x) - t;
  }
  long double diff_aim(long double x, long double s){
    return s-2*x;
  }

  long double diff_maxInv(long double x, long double s){
    return (s-x)*(s-x)/4-x*(s-x)/2;
  }
  long double diff_minInv(long double x, long double s){
    return EPSILON;
  }

  ProductFunction(){}
};
#endif
