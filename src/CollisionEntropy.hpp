#ifndef __COLLISION_ENTROPY_HPP__
#define __COLLISION_ENTROPY_HPP__

#include"SumConcaveFunction.hpp"


class CollisionEntropy: public SumConcaveFunction{
public:

  int sign(){
    return -1;
  }
  
  long double g(long double x, int k){
    return x*x;
  }
  
  long double h(long double y, int k){
    return -log2(y);
  }

  long double h_inv(long double y, int k){
    return powl(2,-y);
  }
  
  long double aim_with_target(long double x, long double s, long double t){
    return -log(x*x+(s-x)*(s-x)) - t;
  }
  
  long double diff_aim(long double x, long double s){
    return -(4*x-2*s)/((x*x+(s-x)*-(s-x))*log(2));
  }

  long double diff_maxInv(long double x, long double s){
    return -(3*x-s)/((x*x+(s-x)*(s-x)/2)*log(2));
  }

  long double diff_minInv(long double x, long double s){
    return -(4*x-2*s+2*EPSILON)/((x*x+(s-x-EPSILON)*(s-x-EPSILON)+EPSILON*EPSILON)*log(2));
  }

  CollisionEntropy(){}
  
};
#endif

