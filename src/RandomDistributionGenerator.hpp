#ifndef __RANDOM_DIST_GENERATOR_HPP
#define __RANDOM_DIST_GENERATOR_HPP
#include<iostream>
#include"ConcaveFunction.hpp"
#include<assert.h>
#include<unistd.h>
#include<stdexcept>
#include <iomanip>

template<typename F = ConcaveFunction>
class RandomDistributionGenerator{
private:
  long int __AIM_STEPS__;
  long int __FIRST_STEP_FAILURES__;
  long int __INC_DIST_FAILURES__;
  long int __MARKOV_STEP_FAILURES__;
  int __FIRST_STATE_ITERATIONS__;
  int __NB_XP__;
  long int __NEWTON_STEPS__;
  long int __NEWTON_CALLS__;

  F cf;


  void distribution_with_max_concave(long double * res, int n){
    int i;
    for(i=0; i < n; i++)
      res[i] = 1/(long double)n;
  }

  void add_delta(long double * distribution, int size, long double delta){
    int i;
    for(i = 0; i < size; i++)
      distribution[i] += delta;
  }

  void random_triplet(int max_value, int * res){
    res[0] = rand()%max_value;
    do{ res[1] = rand()%max_value;} while(res[0] == res[1]);
    do{ res[2] = rand()%max_value;} while(res[0] == res[2] || res[1] == res[2]);
  }
  
  long double sum_distribution(long double * dist, int k){
    long double res = 0.;
    int i;
    for(i=0; i < k; i++)
      res += dist[i];
    return res;
  }

  long double contribution_except_two(long double * dist, int k, int first, int second){
    long double res = 0.;
    int i;
    for(i=0; i < k; i++){
      if( i != first && i != second )
	res += cf.g(dist[i], k);
    }
    return res;    
  }
  
  long double aim_concave_value(long double sum, long double target){
    long double p = sum/2;
    long double step = p/2.;
    long double aim_p = cf.aim_double(p, sum-p);
    // If the target is unreachable, exit function, only happens when precision error occurs
    //assert(sum<=1);
    if((double)aim_p < (double)target || (double)cf.aim_double(EPSILON, sum-EPSILON) > (double)target) {
      return -1.;
    }
    while ((double)step > (double)EPSILON && (double) aim_p != (double) target && p > 0){
      if(aim_p < target)
	p += step;
      else
	p -= step;
      aim_p = cf.aim_double(p, sum-p);
      step /= 2.;
      __AIM_STEPS__++;
    }
    // the precision for aim is lower than for probabilities.
    if((float)aim_p!=(float)target){
      return -1;
    }
    
    if(rand()%2)
      return p;
    return sum-p;
  }
  
  void distribution_markov_chain(long double * res, long double target, int size, int steps){
    int i;

    while( reach_first_state(res, target, size) < 0){
      __FIRST_STEP_FAILURES__++;
      if(__FIRST_STEP_FAILURES__ == 10)
	throw (target);
    }

    for (i = 0; i < steps; i++)
      markov_chain_step(res, size);        
  }

  
  
  int reach_first_state(long double * distribution, long double target, int size){
    long double incomplete_sum, contribution;
    long double min_aim, max_aim, update;
    long double delta = 0.5/(long double)size;
    distribution_with_max_concave(distribution, size);
    incomplete_sum = distribution[0]*(size-2);
    distribution[size-2] = (1-incomplete_sum)/2;
    distribution[size-1] = (1-incomplete_sum)/2;
    max_aim = cf.h(cf.contribution(distribution, size), size);
    distribution[size-2] = (1-incomplete_sum-EPSILON);
    distribution[size-1] = EPSILON;
    min_aim = cf.h(cf.contribution(distribution, size), size);
    
    while( (max_aim < target || min_aim > target) && delta > EPSILON && (double)max_aim != (double)target && (double)min_aim != (double)target){
      __FIRST_STATE_ITERATIONS__++;

      if( min_aim > target )
	add_delta(distribution, size-2, -delta);
      else
	add_delta(distribution, size-2, delta);
      
      incomplete_sum = distribution[0]*(size-2);
      distribution[size-2] = (1-incomplete_sum)/2;
      distribution[size-1] = (1-incomplete_sum)/2;
      max_aim = cf.h(cf.contribution(distribution, size),size);
      distribution[size-2] = (1-incomplete_sum-EPSILON);
      distribution[size-1] = EPSILON;
      min_aim = cf.h(cf.contribution(distribution, size),size);

      delta/=2;
    }
    contribution = cf.contribution(distribution, size-2);
    update = cf.update_target(target, contribution, size, size-2);
    distribution[size-2] = aim_concave_value(1-incomplete_sum, update);
    
    if(distribution[size-2] < 0)
      return -1;
  										
    distribution[size-1] = 1-incomplete_sum - distribution[size-2];
    return 0; //If equal to -1, then a problem occured.
  }


  

  void markov_chain_step(long double * dist, int k){
    int triplet[3], i;
    long double target;
    long double sum;
    long double step[3];
    random_triplet(k, triplet);
    sum = dist[triplet[0]] + dist[triplet[1]] + dist[triplet[2]];
    target = cf.aim_triple(dist[triplet[0]],dist[triplet[1]], dist[triplet[2]]);
    step[0] = cf.random_value_in_approx_interval(sum, target);
    
    if(step[0] < sum){
      step[1] = aim_concave_value(sum - step[0], cf.update(target, step, 3, 1));
      
      if(step[1] != -1){
	step[2] = sum - step[0] - step[1];
	for ( i = 0 ; i < 3; ++i)
	  dist[triplet[i]] = step[i];
      }
      else 
	__MARKOV_STEP_FAILURES__++;
    }
    else
      __MARKOV_STEP_FAILURES__++;
  }
  
public:

  /**
     Print the value of all the metrics on the standard output.
  */
  void print_benchmarks(int nb_xp){
    std::cout<<"Nombre total d'étapes "<<__AIM_STEPS__<<std::endl;
    std::cout<<"Nombre moyen d'échecs de tirage de distribution incomplète: "<<__INC_DIST_FAILURES__/(double)(__FIRST_STEP_FAILURES__+1)<<std::endl;
    std::cout<<"Nombre moyen de rejets sur un pas Markovien: "<<__MARKOV_STEP_FAILURES__/(double)nb_xp<<std::endl;
    std::cout<<"Nombre moyen de d'essai: "<<__FIRST_STEP_FAILURES__/(double)nb_xp<<std::endl;
    std::cout<<"Nombre moyen de multiplication par un facteur "<<__FIRST_STATE_ITERATIONS__/(double)nb_xp<<std::endl;
  }
  
  void random(long double * res, long double target_entropy, int size, int steps){
    __NB_XP__++;
    distribution_with_max_concave(res, size);
    long double max = cf.aim(res, size);
    if(target_entropy < max){
      if(size > 2)
	distribution_markov_chain(res, target_entropy, size, steps);
      else{
	res[0] = aim_concave_value(1, target_entropy);
	res[1] = 1-res[0];
      }
    }
    else
      std::cerr<<"The target is too big to be reached"<<std::endl;
  }

  /**
   Initializes all the metrics of the generator's benchmarks.
   @param nb_xp is the number of conducted experiments
   @ensures all metrics are set to zero, except for the number of experiments
  */
  void init_benchmarks(){
    __AIM_STEPS__ = 0;
    __FIRST_STEP_FAILURES__ = 0;
    __INC_DIST_FAILURES__ = 0;
    __MARKOV_STEP_FAILURES__ = 0;
    __FIRST_STATE_ITERATIONS__ = 0;
    __NB_XP__ = 0;
    __NEWTON_STEPS__ = 0;
    __NEWTON_CALLS__ = 0;
  }


  void print_benchmarks(){
    print_benchmarks(__NB_XP__);
  }

  void print_distribution(long double * dist, int n){
    int i;
    for (i = 0; i< n; i++){
        printf("%Lg ", dist[i]);
    }
    printf("\n");
  }
  
  RandomDistributionGenerator(){    
    init_benchmarks();
  }

  /********************** PROVING EFFICIENCY **********************/

long double compute_minInv_interval(long double s, long double t, long double start){
  return cf.newton(F::minInv, F::diff_minInv, s,t, EPSILON, 30);
}

void compute_interval_of_values(long double s, long double t,
				long double * maxInvMin, long double * maxInvMax){
  *maxInvMin = newton(F::maxInv, F::diff_maxInv, s,t, s/10, 30);
  *maxInvMax = newton(F::maxInv, F::diff_maxInv, s,t, 9*s/10, 30);
}

  
void controlled_step(long double * mu, long double nu0, int pos){
  long double sum = mu[0] + mu[pos] + mu[pos+1];
  long double target = aim_triple(cf.aim, mu[0], mu[pos], mu[pos+1]);
  long double maxInvMin, maxInvMax;
  long double mu0 = mu[0];
  long double aim;
  compute_interval_of_values(sum, target, &maxInvMin, &maxInvMax);
  if( nu0 > maxInvMax )
    mu[0] = maxInvMax;
  else if( target >= cf.maximal_of_two_values(sum) )
    mu[0] = ( nu0 < maxInvMin )? maxInvMin: nu0;
  else{
    long double temp = compute_minInv_interval(sum, target, mu[0]);
    if( nu0 < temp )
      mu[0] = nu0;
    else if( nu0 > (sum-temp) ) 
      mu[0] = nu0;
    else
      mu[0] = (fabsl(temp-nu0) < fabsl((sum-temp)-nu0))? temp: sum-temp;
  }
  if( (aim = cf.aim_concave_value(sum - mu[0], cf.update_target(target, mu[0], 3) )) != -1){
    mu[pos] = aim;
    mu[pos+1] = sum - mu[0] - mu[pos];
  }
  else
    mu[0] = mu0;
}

int closest_value_position(long double * mu, long double value, int k){
  int pos_min = 0;
  for(int i = 1; i < k; i++){
    if( fabsl(mu[i]-value) < fabsl(mu[pos_min]-value) )
      pos_min = i;
  }
  return pos_min;
}

void swap(long double * x, long double * y){
  long double tmp = *x;
  *x = *y;
  *y = tmp;
}

int cmp(const void * x,const void * y){  
  return *(long double *)x-*(long double *)y;
}



int shortest_path(long double * mu, long double * nu, int k){
  if(k == 1)
    return 0;
  if(mu[0] == nu[0])
    return shortest_path(mu+1, nu+1, k-1);
  int pos_min = closest_value_position(mu, nu[0], k);
  swap(&mu[0], &mu[pos_min]);

  int counter = 1;
  while(mu[0] != nu[0] && counter < (k-1)){
    controlled_step(mu, nu[0], counter);
    //print_distribution(mu, k);
    assert(mu[1] != -1);
    counter++;
  }
  
  return counter + shortest_path(mu+1, nu+1, k-1);
}


};

#endif


int draw_according_to_distribution(long double * distribution, int max_value){
  long double p = drand48();
  int res = 0;
  while(p >= distribution[res] && res < max_value-1){
    p -= distribution[res];
    res++;
  }
  return res;
}
