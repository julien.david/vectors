#ifndef __SHANNON_ENTROPY_HPP__
#define __SHANNON_ENTROPY_HPP__

#include"SumConcaveFunction.hpp"

class ShannonEntropy: public SumConcaveFunction{
public:

  int sign(){
    return 1;
  }
  
  long double g(long double x, int k){
    return -x*log2(x);
  }
  
  long double h(long double x, int k){
    return x;
  }

  long double h_inv(long double x, int k){
    return x;
  }
  
  long double aim_with_target(long double x, long double s, long double t){
    return -x*log(x)/log(2) - (s-x)*log(s-x)/log(2) - t;
  }
  
  long double diff_aim(long double x, long double s){
    return (-log(x)+log(s-x))/log(2);
  }

  long double diff_maxInv(long double x, long double s){
    return (-log(x)+log(s-x))/log(2) - 1;
  }
  
  long double diff_minInv(long double x, long double s){
    return (-log(x)+log(s-x-EPSILON))/log(2);
  }
  
  ShannonEntropy(){}
  
};
#endif

