#ifndef __PRODUCT_FUNCTION_HPP__
#define __PRODUCT_FUNCTION_HPP__

#include"ProductConcaveFunction.hpp"


class GeometricMean: public ProductConcaveFunction{
public:
  
  int sign(){
    return 1;
  }

  long double g(long double x, int k){
    return x;
  }
  
  long double h(long double y, int k){
    return powl(y,1./k);
  }

  long double h_inv(long double y, int k){
    return powl(y,k);
  }
  
  long double aim_with_target(long double x, long double s, long double t){
    return sqrt(x*(s-x)) - t;
  }
  long double diff_aim(long double x, long double s){
    return (s-2*x)/(2*sqrt(x*(s-x)));
  }

  long double diff_maxInv(long double x, long double s){
    return ((s-x)*(s-x)-2*(s-x)*x)/(4*sqrt(s*(s-x)*(s-x)));
  }
  long double diff_minInv(long double x, long double s){
    return ((s-x-EPSILON)*EPSILON-x*EPSILON)/(3*powl((x*(s-x-EPSILON)*EPSILON),2/3));
  }

  GeometricMean(){}
};
#endif
