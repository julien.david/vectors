#ifndef __PRODUCT_CONCAVE_FUNCTION_HPP__
#define __PRODUCT_CONCAVE_FUNCTION_HPP__

#include"ConcaveFunction.hpp"
#include<iostream>

class ProductConcaveFunction: public ConcaveFunction{
public:
  long double contribution(long double * t, int n){
    long double prod = 1;
    int i;
    for(i = 0; i < n; i++){
      prod *= g(t[i],n);
    }
    return prod;
  }

  long double update(long double t, long double * x, int k, int l){
    long double c = contribution(x,l);
    return h(h_inv(t, k) / c, k-l);
  }

  long double update_target(long double t, long double x, int k, int l){
    return h(h_inv(t,k) / x, k-l);
  }
  
  long double maximal_of_two_values(long double partial_sum){
    return h(g(partial_sum/2,2)*g(partial_sum/2,2),2);
  }

  long double maxInv(long double x, long double s, long double t){
    return h(g(x,3) * g((s-x)/2,3) * g((s-x)/2,3),3) - t;
  }
  
  long double minimal_of_two_values(long double partial_sum){
    return h(g(partial_sum-EPSILON,2)*g(EPSILON,2),2);
  }

  long double minInv(long double x, long double s, long double t){
    return h(g(x,3) * g(s-x-EPSILON,3) * g(EPSILON,3),3)  -t;
  }

  long double random_value_in_approx_interval(long double s, long double t){
    long double maxInvMin, maxInvMax;
    maxInvMax = newton_max(s,t, 9*s/10, 30);
    if(!(maxInvMax>=0 && maxInvMax <= s))
      maxInvMin = s-2*EPSILON;

    maxInvMin = newton_min(s,t, s/10, 100);
    if(!(maxInvMin>=0 && maxInvMin <= s))
      maxInvMin = EPSILON;
    long double res = drand48();

    //std::cout<<"Min:"<<maxInvMin<<" Max:"<<maxInvMax<<std::endl;
    return res*(maxInvMax-maxInvMin)+maxInvMin;
  }
  
};

#endif
