#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#include"CollisionEntropy.hpp"
#include"ShannonEntropy.hpp"
#include"ProductFunction.hpp"
#include"GeometricMean.hpp"
#include"RandomTextGenerator.hpp"

int main(int argc, char ** argv){
    srand48(time(NULL));
    srand(time(NULL));
    if(argc != 4){
        perror("./random_distribution event_number target length");
        return EXIT_FAILURE;
    }
    int k = atoi(argv[1]);
    long double target = atof(argv[2]);
    int n = atoi(argv[3]);
    char * text = new char[n+1];
    int i;
    int nb_experiment = 10;
    RandomTextGenerator<CollisionEntropy> rtg(k, target);
    
    for(i = 0; i < nb_experiment; i++){
      rtg.random(text, n, false);
      printf("%s\n",text);
    }

    return EXIT_SUCCESS;
}
