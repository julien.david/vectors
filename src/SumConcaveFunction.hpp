#ifndef __SUM_CONCAVE_FUNCTION_HPP__
#define __SUM_CONCAVE_FUNCTION_HPP__

#include"ConcaveFunction.hpp"

class SumConcaveFunction: public ConcaveFunction{
public:
  long double contribution(long double * t, int n){
    long double sum = 0;
    int i;
    for(i = 0; i < n; i++){
      sum += g(t[i],n);
    }
    return sum;
  }

  long double update(long double t, long double * x, int k, int l){
    long double c = contribution(x,l);
    return h(h_inv(t,k) - c, k-l);
  }
  
  long double update_target(long double t, long double x, int k, int l){
    return h(h_inv(t,k) - x, k-l);
  }

  long double maximal_of_two_values(long double partial_sum){
    return h(2*g(partial_sum/2, 1), 2);
  }

  long double maxInv(long double x, long double s, long double t){
    return g(x,1) + maximal_of_two_values(s-x) - t;
  }

  long double minimal_of_two_values(long double partial_sum){
    return h(g(partial_sum-EPSILON,1)+g(EPSILON,1),2);
  }

  long double minInv(long double x, long double s, long double t){
    return g(x,1) + minimal_of_two_values(s-x) -t;
  }

  long double random_value_in_approx_interval(long double s, long double t){
    long double threshold = maximal_of_two_values(s);
    long double maxInvMin, maxInvMax;

    //maxInvMax = newton(maxInv, diff_maxInv, s,t, 9*s/10, 30);
    maxInvMax = newton_max(s,t, 9*s/10, 30);
    
    long double res = drand48();
    if(t < threshold ){
      //      long double lower_approx = newton(minInv, diff_minInv, s,t, s/10, 30);
      long double lower_approx = newton_min(s,t, s/10, 30);
      long double reverse_lower_approx = s - lower_approx;
      long double upper_range = maxInvMax - reverse_lower_approx;
      long double separator = lower_approx/(lower_approx+upper_range);
      if(res >= separator)
	return reverse_lower_approx + drand48()*upper_range;
      else
	return drand48()*lower_approx;
    }
    //maxInvMin = newton(maxInv, diff_maxInv, s,t, s/10, 100);
    maxInvMin = newton_max(s,t, s/10, 100);
    
    return res*(maxInvMax-maxInvMin)+maxInvMin;
  }

  
};

#endif
