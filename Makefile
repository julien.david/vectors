CC = g++
CFLAGS = -Wall -O3

all: random_shannon random_collision random_product random_geometric random_text 


random_product: 
	g++ $(CFLAGS) src/random_product.cpp -o random_product -lm

random_text: 
	g++ $(CFLAGS) src/random_text.cpp -o random_text -lm

random_geometric: 
	g++ $(CFLAGS) src/random_geometric.cpp -o random_geometric -lm

random_collision: 
	g++ $(CFLAGS) src/random_collision.cpp -o random_collision -lm

random_shannon: 
	g++ $(CFLAGS) src/random_shannon.cpp -o random_shannon -lm


clean:
	rm -f src/*.o
	rm -f random_collision
	rm -f random_shannon
	rm -f random_product
	rm -f random_geometric
	rm -f random_text






