# Vectors

This project contains:
Random generators of stochastic vectors with fixed:
 - Shannon Entropy
 - Collision Entropy
 - Product
 - Geometric Mean
And also random text generators whose ditributions on the alphabet have one of
the cited properties.

## What do I need to use it

A g++ compiler

## How to use it

### Compile the project
`make`


### Random stochestic vectors with fixed property
./name_of_the_program [dimension] [target_value]

Let k be the dimension of the vector, please note that
 - Shannon Entropy: t < log(k)/log(2)
 - Collision Entropy: t < log(k)/log(2)
 - Product: t < 1/k**k
 - Geometric Mean: t < 1/k

### Random text generator

The program is currently configured to generate text with a fixed collision entropy.
Modify the declaration of the random generator line 21 of file src/random_text.cpp
to use another random generator.
Possibilities, so far are:
ShannonEntropy, CollisionEntropy, ProductFunction, GeometricMean.

To execute:
./random_text [dimension] [target_value] [text_length]
